package ru.t1.dkononov.tm.api.model;

public interface IHasName {

    String getName();

    void setName(final String name);

}
